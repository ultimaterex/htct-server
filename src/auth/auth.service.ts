import { Injectable } from '@nestjs/common';

import login from 'src/helpers/login';
import { PrismaService } from 'src/prisma/prisma.service';
import { LoginDto } from './dto';

@Injectable()
export class AuthService {
  constructor(private prisma: PrismaService) {}

  async login(dto: LoginDto, ip: string) {
    const { username, userpass } = dto;

    let ipModel = await this.prisma.ip.findFirst({
      where: {
        OR: [
          {
            value: ip,
          },
        ],
      },
    });

    if (!ipModel) {
      ipModel = await this.prisma.ip.create({
        data: {
          value: ip,
        },
      });
    }

    let user = await this.prisma.account.findFirst({
      where: {
        username,
        Ip: {
          OR: [
            {
              value: ip,
            },
            {
              predecessorId: ipModel.id,
            },
          ],
        },
      },
    });

    if (user) {
      return user.loginResponse;
    }

    try {
      const loginResponse = await login(username, userpass, true);

      if (loginResponse.retmsg === 'Thành công') {
        user = await this.prisma.account.create({
          data: {
            username,
            password: userpass,
            loginResponse,
            ipId: ipModel.id,
          },
        });
      }

      return loginResponse;
    } catch (error) {
      console.log('error.response', error.message);
    }
  }
}
