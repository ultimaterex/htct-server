import { Body, Controller, Post, Req } from '@nestjs/common';

import { AuthService } from './auth.service';
import { LoginDto } from './dto';

@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) {}

  @Post('login')
  login(@Body() dto: LoginDto, @Req() req: any) {
    const ip = req.clientIp;

    return this.authService.login(dto, ip);
  }
}
