import axios from 'axios';
import * as md5 from 'md5';

const login = async (
  username: string,
  password: string,
  isEncrypted = false,
  proxy?: {
    ip: string;
    port: string | number;
    username: string;
    password: string;
  },
  deviceType = 'android',
) => {
  const encryptedPassword = isEncrypted ? password : md5(password);
  const options: {
    method: string;
    url: string;
    params: any;
    headers: any;
    data: string;
    proxy?: any;
  } = {
    method: 'POST',
    url: 'http://sdk.api.mplay8.com/',
    params: {
      act: 'user.phonelogin',
      deviceType,
      location: 'vn',
      lang: 'vn',
      pid: '862',
      package: 'com.jsmr.ggplay.htct',
    },
    headers: {
      'user-agent':
        'Dalvik/2.1.0 (Linux; U; Android 7.1.1; Android SDK built for x86_64 Build/NYC) Kunlun Android SDK Version:5.89.1111',
      'content-type':
        'multipart/form-data;boundary=3i2ndDfv2rTHiSisAbouNdArYfORhtTPEefj3q2f',
      connection: 'Keep-Alive',
      host: 'sdk.api.mplay8.com',
      'accept-encoding': 'gzip',
    },
    data: `--3i2ndDfv2rTHiSisAbouNdArYfORhtTPEefj3q2f\r\nContent-Disposition: form-data; name="method"\r\n\r\nPOST\r\n--3i2ndDfv2rTHiSisAbouNdArYfORhtTPEefj3q2f\r\nContent-Disposition: form-data; name="username"\r\n\r\n${username}\r\n--3i2ndDfv2rTHiSisAbouNdArYfORhtTPEefj3q2f\r\nContent-Disposition: form-data; name="userpass"\r\n\r\n${encryptedPassword}\r\n--3i2ndDfv2rTHiSisAbouNdArYfORhtTPEefj3q2f\r\nContent-Disposition: form-data; name="u"\r\n\r\n\r\n--3i2ndDfv2rTHiSisAbouNdArYfORhtTPEefj3q2f\r\nContent-Disposition: form-data; name="u2"\r\n\r\n\r\n--3i2ndDfv2rTHiSisAbouNdArYfORhtTPEefj3q2f\r\nContent-Disposition: form-data; name="mac"\r\n\r\n02:00:00:44:55:66\r\n--3i2ndDfv2rTHiSisAbouNdArYfORhtTPEefj3q2f\r\nContent-Disposition: form-data; name="imei"\r\n\r\n\r\n--3i2ndDfv2rTHiSisAbouNdArYfORhtTPEefj3q2f\r\n\r\n--3i2ndDfv2rTHiSisAbouNdArYfORhtTPEefj3q2f\r\n`,
  };

  if (proxy) {
    options.proxy = {
      host: proxy.ip,
      port: proxy.port,
      auth: {
        username: proxy.username,
        password: proxy.password,
      },
    };
  }

  const response = await axios.request(options);

  return response.data;
};

export default login;
