import { Injectable } from '@nestjs/common';
import * as md5 from 'md5';

import login from 'src/helpers/login';
import { PrismaService } from 'src/prisma/prisma.service';
import { ScanAccountDto } from './dto';

@Injectable()
export class AccountService {
  constructor(private prisma: PrismaService) {}

  async scan(dto: ScanAccountDto, ip: string) {
    const { username, password, proxy } = dto;
    let formattedProxy;

    let ipModel = await this.prisma.ip.findFirst({
      where: {
        OR: [
          {
            value: ip,
          },
        ],
      },
    });

    if (!ipModel) {
      ipModel = await this.prisma.ip.create({
        data: {
          value: ip,
        },
      });
    }

    let user = await this.prisma.account.findFirst({
      where: {
        username,
        Ip: {
          OR: [
            {
              value: ip,
            },
            {
              predecessorId: ipModel.id,
            },
          ],
        },
      },
    });

    if (user) {
      return {
        message: 'Success',
      };
    }

    if (proxy) {
      const [ip, port, username, password] = proxy.split(':');

      formattedProxy = {
        ip,
        port,
        username,
        password,
      };
    }

    try {
      const loginResponse = await login(
        username,
        password,
        false,
        formattedProxy,
      );

      if (loginResponse.retmsg === 'Thành công') {
        user = await this.prisma.account.create({
          data: {
            username,
            password: md5(password),
            loginResponse,
            ipId: ipModel.id,
          },
        });

        return {
          message: 'Success',
        };
      } else {
        return {
          message: loginResponse.retmsg,
        };
      }
    } catch (error) {
      console.log('error.response', error.message);
    }
  }
}
