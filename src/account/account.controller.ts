import { Body, Controller, Post, Req } from '@nestjs/common';

import login from 'src/helpers/login';
import { AccountService } from './account.service';
import { ScanAccountDto } from './dto';

@Controller('account')
export class AccountController {
  constructor(private accountService: AccountService) {}

  @Post('scan')
  scan(@Body() dto: ScanAccountDto, @Req() req: any) {
    const ip = req.clientIp;

    return this.accountService.scan(dto, ip);
  }
}
