import { IsNotEmpty, IsOptional, IsString } from 'class-validator';

export class AccountDto {
  @IsString()
  @IsNotEmpty()
  username: string;

  @IsOptional()
  @IsString()
  password?: string;
}

export class ScanAccountDto extends AccountDto {
  @IsOptional()
  @IsString()
  proxy?: string;
}
